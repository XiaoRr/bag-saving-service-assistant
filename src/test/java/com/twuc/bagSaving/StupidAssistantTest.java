package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.twuc.bagSaving.CabinetFactory.createCabinetWithFullLockers;
import static com.twuc.bagSaving.CabinetFactory.createCabinetWithPlentyOfCapacity;
import static org.junit.jupiter.api.Assertions.*;

class StupidAssistantTest {
    @Test
    void should_get_ticket_when_save_bag_by_assistant() {
        StupidAssistant assistant = new StupidAssistant(createCabinetWithPlentyOfCapacity());
        Ticket ticket = assistant.save(new Bag(BagSize.MEDIUM));
        assertNotNull(ticket);
    }

    @Test
    void should_get_bag_when_give_ticket() {
        StupidAssistant assistant = new StupidAssistant(createCabinetWithPlentyOfCapacity());
        Bag bag1 = new Bag(BagSize.MEDIUM);
        Ticket ticket = assistant.save(bag1);
        Bag bag2 = assistant.getBag(ticket);
        assertEquals(bag1, bag2);
    }

    @Test
    void should_throw_exception_when_space_full() {
        StupidAssistant assistant = new StupidAssistant(createCabinetWithFullLockers(new LockerSize[]{LockerSize.BIG}, 1));
        assertThrows(
                InsufficientLockersException.class,
            () -> assistant.save(new Bag(BagSize.BIG)));
    }

    @Test
    void should_save_bag_on_first_cabinet() {
        List<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(createCabinetWithPlentyOfCapacity());
        StupidAssistant assistant = new StupidAssistant(cabinets);
        Ticket ticket = assistant.save(new Bag(BagSize.MEDIUM));
        assertNotNull(ticket);
    }

    @Test
    void should_save_bag_on_forth_cabinet() {
        List<Cabinet> cabinets = getFullCabinetsList();
        cabinets.add(createCabinetWithPlentyOfCapacity());
        StupidAssistant assistant = new StupidAssistant(cabinets);
        Ticket ticket = assistant.save(new Bag(BagSize.MEDIUM));
        assertNotNull(ticket);
    }

    private List<Cabinet> getFullCabinetsList() {
        List<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(createCabinetWithFullLockers(new LockerSize[]{LockerSize.MEDIUM}, 1));
        cabinets.add(createCabinetWithFullLockers(new LockerSize[]{LockerSize.MEDIUM}, 1));
        cabinets.add(createCabinetWithFullLockers(new LockerSize[]{LockerSize.MEDIUM}, 1));
        return cabinets;
    }

    @Test
    void should_throw_exception_when_both_cabinet_are_full() {
        List<Cabinet> cabinets = getFullCabinetsList();
        cabinets.add(createCabinetWithFullLockers(new LockerSize[]{LockerSize.MEDIUM},1));
        StupidAssistant assistant = new StupidAssistant(cabinets);
        assertThrows(
                InsufficientLockersException.class,
                () -> assistant.save(new Bag(BagSize.MEDIUM)));
    }

    @Test
    void should_get_bag_in_cabinets(){
        List<Cabinet> cabinets = getFullCabinetsList();
        cabinets.add(createCabinetWithPlentyOfCapacity());
        Bag bag = new Bag(BagSize.MEDIUM);
        StupidAssistant assistant = new StupidAssistant(cabinets);
        Ticket ticket = assistant.save(bag);
        Bag bag2 = assistant.getBag(ticket);
        assertEquals(bag,bag2);
    }

    @Test
    void should_get_bag_by_another_stupid_assistant() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant assistant = new StupidAssistant(cabinet);
        StupidAssistant assistant2 = new StupidAssistant(cabinet);
        Ticket ticket = assistant.save(new Bag(BagSize.MEDIUM));
        assertNotNull(assistant2.getBag(ticket));
    }
}