package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;

import static com.twuc.bagSaving.CabinetFactory.createCabinetFullSmallAndEmptyMedium;
import static org.junit.jupiter.api.Assertions.*;

class LazyAssistantTest {
    @Test
    void should_save_a_medium_bag_in_large_cabinet_correctly() {
        Cabinet cabinet = createCabinetFullSmallAndEmptyMedium();
        LazyAssistant lazyAssistant = new LazyAssistant(cabinet);
        Ticket ticket = lazyAssistant.save(new Bag(BagSize.SMALL));
        assertNotNull(ticket);
    }
}