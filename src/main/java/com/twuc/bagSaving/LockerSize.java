package com.twuc.bagSaving;

public enum LockerSize {
    SMALL(20),
    MEDIUM(30),
    BIG(40);

    private final int sizeNumber;

    LockerSize(int sizeNumber) {
        this.sizeNumber = sizeNumber;
    }

    int getSizeNumber() {
        return sizeNumber;
    }

    public static LockerSize[] returnList(){
        return new LockerSize[]{SMALL, MEDIUM, BIG};
    }
}
