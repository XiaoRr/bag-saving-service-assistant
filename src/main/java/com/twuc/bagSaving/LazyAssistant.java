package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LazyAssistant {
    private List<Cabinet> cabinets = new ArrayList<>();

    private final Map<BagSize,LockerSize> sizeMap = new HashMap<>();

    private void initSizeMap() {
        sizeMap.put(BagSize.BIG,LockerSize.BIG);
        sizeMap.put(BagSize.MEDIUM,LockerSize.MEDIUM);
        sizeMap.put(BagSize.SMALL,LockerSize.SMALL);
    }
    public LazyAssistant(Cabinet cabinet) {
        this.cabinets.add(cabinet);
        initSizeMap();
    }

    public LazyAssistant(List<Cabinet> cabinets) {
        this.cabinets = cabinets;
        initSizeMap();
    }

    public Ticket save(Bag bag) {
        LockerSize size = LockerSize.valueOf(bag.getBagSize().toString());
        for(int i=0;i<cabinets.size();i++){
            for(LockerSize lockerSize: LockerSize.values()){
                try{
                    return cabinets.get(i).save(bag, lockerSize);
                }catch (InsufficientLockersException e){}
            }
        }
        return cabinets.get(cabinets.size()-1).save(bag,LockerSize.BIG);
    }

    public Bag getBag(Ticket ticket) {
        for(int i=0;i<cabinets.size()-1;i++){
            try{
                return cabinets.get(i).getBag(ticket);
            }catch (IllegalArgumentException e){}
        }
        return cabinets.get(cabinets.size()-1).getBag(ticket);
    }

}
