package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StupidAssistant {
    private List<Cabinet> cabinets = new ArrayList<>();

    private final Map<BagSize,LockerSize> sizeMap = new HashMap<>();

    private void initSizeMap() {
        sizeMap.put(BagSize.BIG,LockerSize.BIG);
        sizeMap.put(BagSize.MEDIUM,LockerSize.MEDIUM);
        sizeMap.put(BagSize.SMALL,LockerSize.SMALL);
    }
    public StupidAssistant(Cabinet cabinet) {
        this.cabinets.add(cabinet);
        initSizeMap();
    }

    public StupidAssistant(List<Cabinet> cabinets) {
        this.cabinets = cabinets;
        initSizeMap();
    }

    public Ticket save(Bag bag) {
        if(!sizeMap.containsKey(bag.getBagSize())){
            throw new IllegalArgumentException("stupid assistant can't save this bag's size.");
        }
        for(int i=0;i<cabinets.size()-1;i++){
            try{
                return cabinets.get(i).save(bag, sizeMap.get(bag.getBagSize()));
            }catch (InsufficientLockersException e){}
        }
        return cabinets.get(cabinets.size()-1).save(bag,sizeMap.get(bag.getBagSize()));
    }

    public Bag getBag(Ticket ticket) {
        for(int i=0;i<cabinets.size()-1;i++){
            try{
                return cabinets.get(i).getBag(ticket);
            }catch (IllegalArgumentException e){}
        }
        return cabinets.get(cabinets.size()-1).getBag(ticket);
    }
}
